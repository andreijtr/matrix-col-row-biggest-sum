package com.sda.matrix.iostream;

import java.io.*;

public class NumberWriter {

    private String outputMessage;

    public NumberWriter(String outputMessage) {
        this.outputMessage = outputMessage;
    }

    public String getOutputMessage() {
        return outputMessage;
    }

    public void setOutputMessage(String outputMessage) {
        this.outputMessage = outputMessage;
    }

    public void writeNumber(int number, String path) {

        File file = new File(path);
        try {

            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(this.outputMessage);
            bufferedWriter.write(" " + number);
            bufferedWriter.close();

            System.out.println("Number was written in " + path);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
