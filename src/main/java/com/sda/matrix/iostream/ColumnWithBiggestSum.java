package com.sda.matrix.iostream;

public class ColumnWithBiggestSum {

    public int findColumn(int[][] matrix) {

        int columnWithBiggestSum = 0;
        int sumOfColumnWithBiggestSum = Integer.MIN_VALUE;

        //parse every column, compute sum and then compare with the biggest sum
        //if current sum is bigger, update the biggest column and biggest sum
        for (int currentColumn = 0; currentColumn < matrix[0].length; currentColumn++) {
            int sumOfCurrentColumn = 0;

            for (int j = 0; j < matrix.length; j++) {
                sumOfCurrentColumn += matrix[j][currentColumn];
            }

            if (sumOfCurrentColumn > sumOfColumnWithBiggestSum) {
                columnWithBiggestSum = currentColumn;
                sumOfColumnWithBiggestSum = sumOfCurrentColumn;
            }

        }
        //must add 1, because Java is counting from 0 to n (not inclusive)
        return columnWithBiggestSum + 1;
    }
}
