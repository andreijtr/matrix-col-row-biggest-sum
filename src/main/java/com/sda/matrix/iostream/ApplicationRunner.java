package com.sda.matrix.iostream;

public class ApplicationRunner {

    public static void main(String[] args) {

        MatrixReader reader = new MatrixReader();

        //create one object to find the biggest row, and another one to find the biggest column
        RowWithBiggestSum finderOfRow = new RowWithBiggestSum();
        ColumnWithBiggestSum finderColumn = new ColumnWithBiggestSum();

        //create two objects that will write the results in different files
        NumberWriter writeRow = new NumberWriter("Row with biggest sum is row number");
        NumberWriter writeColumn = new NumberWriter("Column with biggest sum is column number");

        //read the matrix from file
        int[][] matrixFromFile = reader.readMatrix("D:\\Andrei\\Java\\FisiereIO\\matrice.txt");

        //find the row with biggest sum
        int row = finderOfRow.findRow(matrixFromFile);

        //write found number in file biggestRow.txt
        writeRow.writeNumber(row,"D:\\Andrei\\Java\\FisiereIO\\biggestRow.txt");

        //find the row with biggest sum
        int col = finderColumn.findColumn(matrixFromFile);

        //write found number in file biggestColumn.txt
        writeColumn.writeNumber(col,"D:\\Andrei\\Java\\FisiereIO\\biggestColumn.txt");

    }
}
