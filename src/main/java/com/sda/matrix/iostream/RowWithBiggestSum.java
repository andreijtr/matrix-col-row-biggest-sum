package com.sda.matrix.iostream;

public class RowWithBiggestSum {

    public int findRow(int[][] matrix) {

        int rowWithBiggestSum = 0;
        int sumOfRowWithBiggestSum = Integer.MIN_VALUE;

        for (int i = 0; i < matrix.length; i++) {
            int sumOfCurrentRow = 0;

            for (int j = 0; j < matrix[i].length; j++) {
                sumOfCurrentRow += matrix[i][j];
            }

            if (sumOfCurrentRow > sumOfRowWithBiggestSum) {
                rowWithBiggestSum = i;
                sumOfRowWithBiggestSum = sumOfCurrentRow;
            }
        }

        return rowWithBiggestSum + 1;
    }
}
