package com.sda.matrix.iostream;

import org.junit.Assert;
import org.junit.Test;

public class RowWithBiggestSumTest {

    @Test
    public void findRowTest() {

        RowWithBiggestSum rowTest = new RowWithBiggestSum();

        int[][] matrix = {{1, 2, 4, 4}, {9, 13, 22, 122}, {0, 12, 89, 5}};

        int expectedNumber = 2;
        int actualNumber = rowTest.findRow(matrix);

        Assert.assertEquals(expectedNumber,actualNumber);
    }
}
