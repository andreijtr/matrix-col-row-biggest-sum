package com.sda.matrix.iostream;

import org.junit.Assert;
import org.junit.Test;

public class ColumnWithBiggestSumTest {

    @Test
    public void findColumnTest() {

        ColumnWithBiggestSum rowTest = new ColumnWithBiggestSum();

        int[][] matrix = {{1, 2, 4, 4}, {9, 13, 22, 122}, {0, 12, 89, 5}};

        int expectedNumber = 4;
        int actualNumber = rowTest.findColumn(matrix);

        Assert.assertEquals(expectedNumber,actualNumber);
    }
}
